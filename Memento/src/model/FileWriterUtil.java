/* E' usato quando vogliamo salvare lo stato di un oggetto per recuperarlo in seguito. Memento implementa questo
 * in modo che salvando lo stato non è accessibile esternamente, questo fa si che sia mantenuta l'integrità dello
 * stato. Vengono implementate due classi: Originator e Caretaker. Il primo è quello di cui dobbiamo salvare lo
 * stato, utilizza un'inner class per far ciò. Questa classe viene chiamata Memento ed è privata. Caretaker è
 * la classe che ha il compito di richiedere il salvataggio e il caricamento dello stato di Originator. 
 * Memento è facile da implementare. Bisogna solamente assicurarsi che la classe Memento non si accessibile
 * al di fuori dell'Originator e che sia fornita una classe Caretaker al client per fare le operazioni di 
 * salvataggio e caricamento. 
 * */
package model;

public class FileWriterUtil {

	private String fileName;
	private StringBuilder content;
	
	public FileWriterUtil(String file){
		this.fileName=file;
		this.content=new StringBuilder();
	}
	
	@Override
	public String toString(){
		return this.content.toString();
	}
	
	public void write(String str){
		content.append(str);
	}
	
	public Memento save(){
		return new Memento(this.fileName,this.content);
	}
	
	public void undoToLastSave(Object obj){
		Memento memento = (Memento) obj;
		this.fileName= memento.fileName;
		this.content=memento.content;
	}
	
	public void undoToLastSaveWithOverwrite(Object obj){
		String tempFileName=this.fileName;
		StringBuilder tempContent=this.content;
		Memento memento = (Memento) obj;
		this.fileName= memento.fileName;
		this.content=memento.content;
		memento.fileName=tempFileName;
		memento.content=tempContent;
	}
	
	
	private class Memento{
		private String fileName;
		private StringBuilder content;
		
		public Memento(String file, StringBuilder content){
			this.fileName=file;
			//notice the deep copy so that Memento and FileWriterUtil content variables don't refer to same object
			this.content=new StringBuilder(content);
		}
	}
}