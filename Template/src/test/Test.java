package test;

import model.GlassHouse;
import model.HouseTemplate;
import model.WoodenHouse;

public class Test {

	public static void main(String[] args) {

		HouseTemplate houseType = new WoodenHouse();
		
		//using template method
		houseType.buildHouse();
		System.out.println("************");
		
		houseType = new GlassHouse();
		
		houseType.buildHouse();
	}

}
