/* E' un pattern che definisce gli step per eseguire un algoritmo e fornisce un implementazione di default 
 * comune a tutte (o ad una parte) delle sottoclassi. Nell'esempio simuliamo la costruzione di una casa.
 * Gli step per realizzare una casa sono costruire le fondamenta, i pilastri, i muri e le finestre. Il fatto
 * importante è che non possiamo cambiare l'ordine di costruzione. Quindi viene creato un template method 
 * costante che usa vari metodi. Il metodo di costruzione delle fondamenta e delle finestre è uguale per tutte le case, 
 * quindi viene fornita un'implementazione di base, ciò non toglie che le sottoclassi possano
 * overridare questo metodo. 
 * 
 * Punti importanti:
 * - Il template method deve consistere in steps in un ordine fissato e per alcuni di questi steps(che poi in realtà
 * sono metodi) ci devono essere diverse implementazioni nelle sottoclassi. Il metodo deve essere final.
 * - Molte volte, le sottoclassi chiamano i metodi dall superclasse ma in questo pattern il template method chiama
 * i metodi delle sottoclassi (Hollywood Principle).
 * - I metodi della superclasse con un'implementazione di default sono referenziati come Hooks e possono essere 
 * overridati dalle sotto classi, se si vuole impedire ciò basta dichiararli con il final.
 * 
 * */
package model;

public abstract class HouseTemplate {

	//template method, final cosi le sottoclassi non possono sovrascrivere
	public final void buildHouse(){
		buildFoundation();
		buildPillars();
		buildWalls();
		buildWindows();
		System.out.println("House is built.");
	}

	//implementazione di default
	private void buildWindows() {
		System.out.println("Building Glass Windows");
	}

	public abstract void buildWalls();
	public abstract void buildPillars();

	private void buildFoundation() {
		System.out.println("Building foundation with cement,iron rods and sand");
	}
}
