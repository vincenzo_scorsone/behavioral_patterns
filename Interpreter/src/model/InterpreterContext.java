/* Viene usato per definire una rappresentazione grammaticale di un linguaggio e per fornire un interprete di 
 * questo. Il miglior esempio è il compilatore che interpreta il codice java in bytecode per poter essere 
 * utilizzato dalla JVM. Un altro esempio potrebbe essere un comune traduttore(es. Google Translate). Per
 * implementare questo pattern bisogna creare un Context che svolgerà il lavoro il compito di interpretare 
 * e poi creare una differente implementazione di Expression per utilizzare le funzionalità fornite nel
 * Context. Il client prenderà un input dall'utente, deciderà che Expression utilizzare e genererà un output.
 * 
 * Punti interessanti:
 * - Può essere usato quando dobbiamo creare un albero sintattico per la grammatica che vogliamo
 * - Richiede molti check per gli errori e molte espressioni e codice per valutarli, diventa difficile
 * al crescere della complessità della grammatica
 * */
package model;

public class InterpreterContext {
	 
    public String getBinaryFormat(int i){
        return Integer.toBinaryString(i);
    }
     
    public String getHexadecimalFormat(int i){
        return Integer.toHexString(i);
    }
}