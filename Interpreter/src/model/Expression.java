package model;

public interface Expression {
	 
    String interpret(InterpreterContext ic);
}
