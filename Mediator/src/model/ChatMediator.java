/* E' molto utile nell'applicazioni enterprise dove molti oggetti interagiscono con altri. Se gli oggetti
 * interagiscono direttamente, i componenti del sistema sono strettamente accoppiati e ciò rende difficile
 * la manutenibilità e l'estensione. Mediator fornisce un mediatore tra gli oggetti disaccopiandoli. 
 * I controllori del traffico aereo sono un ottimo esempio di Mediator. Gli oggetti del sistema che comunicano
 * reciprocamente sono chiamati Colleagues. Di solito abbiamo un'interfaccia o una classe astratta che fornisce
 * un contratto di comunicazione e un'implementazione concreta di questa. In questo esempio simuliamo 
 * un'implementazione di una chat di gruppo. 
 * 
 * Punti importanti:
 * - E' utile quando la comunicazione logica tra oggetti è complicata, permette di avere un componente 
 * centrale per gestire la comunicazione.
 * - Java Message Service (JMS) usa il pattern Mediator con Observer per permettere alle applicazioni di 
 * sovrascrivere e pubblicare dati alle altre applicazioni
 * - Non dovremmo utilizzare il Mediator quando il numero cresce perchè rende il tutto difficile da
 * gestire
 * */
package model;

public interface ChatMediator {

	public void sendMessage(String msg, User user);

	void addUser(User user);
}