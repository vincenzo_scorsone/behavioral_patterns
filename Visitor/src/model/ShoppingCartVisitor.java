/* Il Visitor è utilizzato quando vogliamo fare delle operazioni su un gruppo di oggetti di tipo simile.
 * Con il Visitor possiamo spostare le operazioni logiche dagli oggetti ad un'altra classe. Nell'esempio
 * implementiamo uno ShoppingCart che può aggiungere differenti Items. Quando clicchiamo su checkout,
 * viene calcolato l'importo totale da pagare. Potremmo far effettuare questo calcolo nelle classi che
 * rappresentano gli Items oppure possiamo spostarlo nella classe Visitor. 
 * 
 * Benefici:
 * Il beneficio è che se le operazioni logiche cambiano bisogna cambiare la classe visitor invece che 
 * farlo in tutte le classi Items. Un altro beneficio è che aggiungere un nuovo Item è facile. 
 * 
 * Limitazioni:
 * Lo svantaggio è che se dobbiamo conoscere il tipo di ritorno di visit() nel momento in cui si definisce 
 * la classe altrimenti bisogna cambiare l'interfaccia e le implementazioni. Un'altra limitazione è che
 * se ci sono molte implementazioni di visitor, diventa difficile da estendere.
 * */
package model;

public interface ShoppingCartVisitor {

	int visit(Book book);
	int visit(Fruit fruit);
}