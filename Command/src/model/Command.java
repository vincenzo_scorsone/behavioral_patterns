/* In questo patterns la richiesta è inviata dall'invoker che la passa al command object.
 * Il command passa la richiesta al metodo appropiato del Receiver. Il client crea il receiver e lo allega
 * al command. Poi crea l'invoker e lo allega al command per fare un'azione. Nell'esempio si vuole creare 
 * un programma che permette di  aprire, scrivere e chiudere file. Questo gestoredi file system deve
 * lavorare su due OS (Win e Unix). Per implementarlo bisogna creare la classe Receiver che farà tutto
 * il lavoro. 
 * 
 * Punti interessanti:
 * - Command è il cuore del pattern definendo il contratto per l'implementazione
 * - L'implementazione di Receiver è separata dall'implementazione di Command
 * - L'implementazione di Command scegli il metodo per invocare sull'oggetto Receiver. Svolge il ruolo
 * di ponte tra il Receiver e i metodi che effettuano le action.
 * - La classe Invoker deve inoltrare la richiesta dal client all'oggetto Command.
 * - Il client è responsabile di istanziare il Command appropiato e l'implementazione del Receiver e poi
 * di associarli.
 * - Il client è responsabile di istanziare l'invoker e associarlo al Command
 * - E' facilmente estensibile, possiamo aggiungere un nuovo action method nel receiver a creare
 * una nuova implementazione di Command senza cambiare il codice del Client.
 * - Lo svantaggio è che il codice diventa confusionario se ci sono molti action method.
 * */
package model;

public interface Command {

	void execute();
}
