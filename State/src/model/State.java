/* E' usato quando un oggetto cambia il suo comportamento basato su uno stato interno. Se bisogna cambiare lo stato di un oggetto
 * bisogna avere una variabile di stato nell'oggetto. Si può usare if-else per effettuare diverse azioni con questa variabile. Il
 * pattern State disaccoppia queste azioni con un'interfaccia State e una classe Context. Context è la classe che ha uno State 
 * riferito a una concreta implementazione della State. Nell'esempio si vuole implementare una TV. Con lo stato ON/OFF. Con questo
 * pattern è difficile commettere errori ed è facile aggiungere nuovi stati. E' simile allo Strategy.
 * */
package model;

public interface State {

	public void doAction();
}