package test;

import model.State;

import model.TVContext;
import model.TVStartState;
import model.TVStopState;

public class Test {

	public static void main(String[] args) {
		TVContext context = new TVContext();
		State tvStartState = new TVStartState();
		State tvStopState = new TVStopState();
		
		context.setState(tvStartState);
		context.doAction();
		
		
		context.setState(tvStopState);
		context.doAction();
		

	}

}
