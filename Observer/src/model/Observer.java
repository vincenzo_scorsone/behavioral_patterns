/* L'observer è utilizzato quando si è interessati allo stato di un oggetto e si vuole
 * notificare un cambiamento di esso. L'oggetto che osserva è chiamato Observer mentre
 * quello che viene osservato è chiamato Subject. La definizione dalla Gang of Four è
 * "Definisce un dipendenza one-to-many tra oggetti così quando un oggetto cambia stato,
 *  tutti quelli che ne dipendono vengono notificati e aggiornati automaticamente".
 * IL Subject contiene una lista di observers per notificare i cambiamenti di stato,
 * così fornisce metodi con i quali l'observer può registrare o deregistrare se stesso.
 * Contiene anche un metodo per notificare i cambiamenti agli osservatori o anche un 
 * metodo per dare gli aggiornamenti.
 * L'Observer ha un metodo per settare l'oggetto da osservare e un altro metodo usato
 * dal Subject per notificare gli aggiornamenti. Java fornisce una classe e un'interfaccia
 * per implementare il pattern ma sono poco usate perchè l'implementazione è semplice anche  
 * senza il loro utilizzo. 
 * Java Message Service (JMS) utilizza questo pattern con il Mediator per permeterre alle
 * applicazioni di sovrascrivere e pubblicare dati in altre applicazioni.
 * MVC utilizza questo pattern dove il Model è il Subject e le VIews sono gli Observer.
 * 
 * */
package model;

public interface Observer {
	
	//method to update the observer, used by subject
	public void update();
	
	//attach with subject to observe
	public void setSubject(Subject sub);
}
