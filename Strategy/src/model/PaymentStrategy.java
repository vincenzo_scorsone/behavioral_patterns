/* Lo strategy è utilizzato quando ci sono più algoritmi per un task e il client decide quale
 * utilizzare a runtime. 
 * */
package model;

public interface PaymentStrategy {

	public void pay(int amount);
}