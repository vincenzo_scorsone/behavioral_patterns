/* Chain of Responsibility è usato per raggiungere il disaccoppiamento in un software dove una richiesta del client
 * è passata attraverso una catena di oggetti che la processano. Gli oggetti stessi decideranno chi processa la richiesta
 * e se la richiesta verrà inviata al prossimo oggetto della catena o meno. Nell'esempio è simulato uno sportello ATM. 
 * L'utente inserisce l'ammontare del prelievo e la macchina lo dispensa in banconote da 50, 20, 10(seguendo questo ordine). Una richiesta
 * che non è multiplo di 10 viene considerata errore
 * 
 * Aspetti importanti: 
 * - Il client non sa quale parte della catena processa la richiesta, si limita solo a mandare la richiesta che verrà presa 
 * nella prima parte della catena
 * - Ogni oggetto nella catena ha la propria implementazione per processar la richiesta, o in parte o completa o p
 * per mandare la richiesta al prossimo oggetto.
 * - Ogni oggetto nella catena deve avere il riferimento al prossimo oggetto nella catena. 
 * - Creare la catena in modo accurato è importante perchè altrimenti potrebbe capitare che la richiesta non possa essere processata.
 * Nell'esempio viene fatto il check sull'ammontare del dell'input inserito dalll'utente
 * - IL pattern è utile per disaccopiare  ma ciò viene raggiunto con il compromesso di fare molte classi con delle implementazioni
 * comuni.
 * */
package model;


public interface DispenseChain {

	//questo metodo serve per definire il prossimo oggetto nella catena
	void setNextChain(DispenseChain nextChain);
	
	//questo metodo serve per processare la richiesta, nelle varie implementazioni
	//questo metodo proverà a processare la richiesta (completamente o in parte), 
	//quando finisce di processare verrà mandata all'oggetto successivo nella catena
	void dispense(Currency cur);
}