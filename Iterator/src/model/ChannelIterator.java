/* Fornisce un metodo standard per attraversare un gruppo di oggetti. E' spesso utlizzato nelle Collections. 
 * Nell'esempio creo una lista di canali radio e il client li attraversa ad uno ad uno in base al tipo di canale.
 * L'interfaccia Iterator viene implementata da un'inner class in modo che non possa essere utilizzata da altre
 * collection.
 * 
 * */
package model;

public interface ChannelIterator {

	public boolean hasNext();
	
	public Channel next();
}


